to run:

1. source bootstrap.sh to set $PATH and $PYTHONPATH

2. run the cli tool on some prepared data:
runner --load data/records.csv --save test.xml,test.json --display test.html,test.xlsx,terminal

3. run the tests by cd'ing to pythin/tests and running runtests.py



See classDiagram.png for the outline.

Description of design:
-a RecordManager class holds a list of HumanRecord objects

-the RecordManager class has read() and write() methods, which call the interface
defined by BaseIO to perform read/write operations

-the RecordManager class has a render() method, which calls the interface defined
BaseDisplay

-read(), write() and render() all take a plugin name as an argument.  this is passed
to a factory (display.get_plugin(), persistence.get_plugin()), which initialises
and returns the correct class.

-to extend the system with more plugins, you just inherit from BaseIO or BaseDisplay,
implement the interface, and add your plugin to the display or persistence modules,
where they are imported and registered.