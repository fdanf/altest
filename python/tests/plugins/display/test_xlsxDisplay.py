import os
import zipfile
import unittest

from animalTest.plugins.display import xlsxDisplay
from animalTest import dataTypes


class TestHtmlDisplay(unittest.TestCase):
    record1 = dataTypes.HumanRecord()
    record1.id = '1'
    record1.name = 'john doe'
    record1.address = '5 Tree Street'
    record1.phone = '0407324578'
    record1.email = 'johndoe@gmail.com'

    record2 = dataTypes.HumanRecord()
    record2.id = '2'
    record2.name = 'jane doe'
    record2.address = '5 Tree Street'
    record2.phone = '0408763542'
    record2.email = 'doej@gmail.com'

    test_records = [record1, record2]

    testXlsxFile = './plugins/display/data/records.xlsx'
    outputTestFile = './plugins/display/data/test.xlsx'

    def setUp(self):
        self.xlsxDisplay=xlsxDisplay.XlsxDisplay(self.test_records)

    @classmethod
    def tearDownClass(cls):
        os.remove(cls.outputTestFile)

    def test_canRoundTripRecordsToFileAndBack(self):
        self.xlsxDisplay.render(self.outputTestFile)

        zip1 = zipfile.ZipFile(self.testXlsxFile)
        ws1 = zip1.open('xl/worksheets/sheet1.xml')

        zip2 = zipfile.ZipFile(self.testXlsxFile)
        ws2 = zip2.open('xl/worksheets/sheet1.xml')

        self.assertEqual(ws1.read(), ws2.read())
