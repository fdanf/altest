import os
import filecmp
import unittest

from animalTest.plugins.display import htmlDisplay
from animalTest import dataTypes


class TestHtmlDisplay(unittest.TestCase):
    record1 = dataTypes.HumanRecord()
    record1.id = '1'
    record1.name = 'john doe'
    record1.address = '5 Tree Street'
    record1.phone = '0407324578'
    record1.email = 'johndoe@gmail.com'

    record2 = dataTypes.HumanRecord()
    record2.id = '2'
    record2.name = 'jane doe'
    record2.address = '5 Tree Street'
    record2.phone = '0408763542'
    record2.email = 'doej@gmail.com'

    test_records = [record1, record2]

    testHtmlFile = './plugins/display/data/records.html'
    outputTestFile = './plugins/display/data/test.html'

    def setUp(self):
        self.htmlDisplay=htmlDisplay.HtmlDisplay(self.test_records)

    @classmethod
    def tearDownClass(cls):
        os.remove(cls.outputTestFile)

    def test_canRoundTripRecordsToFileAndBack(self):
        self.htmlDisplay.render(self.outputTestFile)
        self.assertTrue(filecmp.cmp(self.testHtmlFile, self.outputTestFile))

