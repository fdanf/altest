import os
import filecmp
import unittest

from animalTest.plugins.persistence import csvIO
from animalTest import dataTypes


class TestCsvIO(unittest.TestCase):
    record1 = dataTypes.HumanRecord()
    record1.id = '1'
    record1.name = 'john doe'
    record1.address = '5 Tree Street'
    record1.phone = '0407324578'
    record1.email = 'johndoe@gmail.com'

    record2 = dataTypes.HumanRecord()
    record2.id = '2'
    record2.name = 'jane doe'
    record2.address = '5 Tree Street'
    record2.phone = '0408763542'
    record2.email = 'doej@gmail.com'

    test_records = [record1, record2]

    inputTestFile = './plugins/persistence/data/records.csv'
    outputTestFile = './plugins/persistence/data/test.csv'

    def setUp(self):
        self.records = []
        self.csvIO = csvIO.CsvIO(self.records)

    @classmethod
    def tearDownClass(cls):
        os.remove(cls.outputTestFile)

    def test_canRoundTripRecordsToFileAndBack(self):
        # initialise records with test data and write to file
        self.csvIO = csvIO.CsvIO(self.test_records)
        self.csvIO.write(self.outputTestFile)
    
        # reset records to null
        self.csvIO.records = []
        self.csvIO.read(self.outputTestFile)
    
        for record in self.csvIO.records:
            i = self.csvIO.records.index(record)
            self.assertEqual(self.test_records[i].to_dict(), record.to_dict())
