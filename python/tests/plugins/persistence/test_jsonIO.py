import os
import filecmp
import unittest

from animalTest.plugins.persistence import jsonIO
from animalTest import dataTypes


class TestJsonIO(unittest.TestCase):
    record1 = dataTypes.HumanRecord()
    record1.id = '1'
    record1.name = 'john doe'
    record1.address = '5 Tree Street'
    record1.phone = '0407324578'
    record1.email = 'johndoe@gmail.com'

    record2 = dataTypes.HumanRecord()
    record2.id = '2'
    record2.name = 'jane doe'
    record2.address = '5 Tree Street'
    record2.phone = '0408763542'
    record2.email = 'doej@gmail.com'

    test_records = [record1, record2]

    inputTestFile = './plugins/persistence/data/records.json'
    outputTestFile = './plugins/persistence/data/test.json'

    def setUp(self):
        self.records = []
        self.jsonIO = jsonIO.JsonIO(self.records)

    @classmethod
    def tearDownClass(cls):
        try:
            os.remove(cls.outputTestFile)
        except:
            pass

    def test_canRoundTripRecordsToFileAndBack(self):
        # initialise records with test data and write to file
        self.jsonIO = jsonIO.JsonIO(self.test_records)
        self.jsonIO.write(self.outputTestFile)

        # reset records to null
        self.jsonIO.records = []
        self.jsonIO.read(self.outputTestFile)

        for record in self.jsonIO.records:
            i = self.jsonIO.records.index(record)
            self.assertEqual(self.test_records[i].to_dict(), record.to_dict())