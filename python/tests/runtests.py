#! /usr/bin/python

import os
import nose

def main():

    os.system('chmod -x plugins/display/*.py')
    os.system('chmod -x plugins/persistence/*.py')
    nose.run()

if __name__ == "__main__":
    main()