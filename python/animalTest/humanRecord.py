from animalTest import settings
from animalTest.plugins import display
from animalTest.plugins import persistence


class RecordManager(object):

    def __init__(self, filepath):
        self.persistence_plugin = None
        self.records = {}
        self.set_persistence_plugin(filepath)



    def display_records(self, display_plugin_name, filepath):
        '''
        Display records using a specified display plugin name.
        '''
        display_plugin = display.get_plugin(display_plugin_name, filepath)
        display_plugin.render()
