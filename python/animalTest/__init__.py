from animalTest import humanRecord
from animalTest.plugins import display
from animalTest.plugins import persistence


class RecordManager(object):

    def __init__(self):
        self.records = []
        self.plugin_registry = {}

    def list_display_plugins(self):
        display_plugin_names = [x.name for x in display.DISPLAY_PLUGINS]
        print 'registered display plugins:', display_plugin_names

    def list_persistence_plugins(self):
        persistence_plugin_names = [x.name for x in persistence.PERSISTENCE_PLUGINS]
        print 'registered persistence plugins:', persistence_plugin_names

    def read(self, filepath, plugin_name):
        """
        @exception: IOError
        """
        read_plugin = persistence.get_plugin(plugin_name, self.records)
        if not read_plugin:
            raise TypeError('No valid read plugin was specified: %s' % plugin_name)

        self.plugin_registry[plugin_name] = read_plugin

        try:
            read_plugin.read(filepath)
        except:
            raise IOError('The file could not be read: %s' % filepath)

    def write(self, filepath, plugin_name):
        """
        @exception: IOError
        """
        if plugin_name in self.plugin_registry.keys():
            write_plugin = self.plugin_registry[plugin_name]
        else:
            write_plugin = persistence.get_plugin(plugin_name, self.records)
            self.plugin_registry[plugin_name] = write_plugin

        if not write_plugin:
            raise TypeError('No valid write plugin was specified: %s' % plugin_name)

        try:
            write_plugin.write(filepath)
        except:
            raise IOError('The file could not be written: %s' % filepath)

    # def render(self, filepath, plugin_name):
    def render(self, filepath=None, plugin_name=None):
        """
        @exception: IOError
        """
        if plugin_name in self.plugin_registry.keys():
            render_plugin = self.plugin_registry[plugin_name]
        else:
            render_plugin = display.get_plugin(plugin_name, self.records)
            self.plugin_registry[plugin_name] = render_plugin

        if not render_plugin:
            raise TypeError('No valid render plugin was specified: %s' % plugin_name)

        try:
            render_plugin.render(filepath)
        except:
            raise IOError('The file could not be written: %s' % filepath)


def _get_file_extension(filepath):
    filepath_tokens = filepath.split('.')

    if len(filepath_tokens) == 1:
        return

    file_ext = filepath_tokens[-1]
    return file_ext