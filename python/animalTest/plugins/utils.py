
def get_plugin(plugin_name, plugins_list, base_plugin, *args):
    """

    @param plugin_name: The name of the requested plugin
    @type plugin_name: str

    @param plugins_list: A list of allowed plugin classes
    @type plugins_list: list

    @param base_plugin: The interface class which plugins must implement

    @return: A plugin instance
    """

    plugin_class = [x for x in plugins_list if x.name == plugin_name]

    if not plugin_class:
        print 'The requested plugin %s was not registered' % plugin_name
        return

    if len(plugin_class) != 1:
        print 'More than one registered plugin has the name %s' % plugin_name
        return

    plugin = plugin_class[0](*args)

    if not isinstance(plugin, base_plugin):
        print 'The plugin %s does not implement the interfacef of the base plugin %s' % (plugin_name, base_plugin)
        return

    return plugin
