import os

import xml.dom.minidom

from animalTest.plugins.persistence import baseIO
from animalTest import dataTypes

class XmlIO(baseIO.BaseIO):

    name = 'xml'

    def __init__(self, records):
        super(XmlIO, self).__init__(records)

    def read(self, filepath):
        print '%s plugin loading %s' % (self.name, filepath)

        with open(filepath, 'r') as f:
            doc = xml.dom.minidom.parse(f)

        dict__ = self._elementToDict(doc.documentElement)['human']

        for record in dict__:
            hr = dataTypes.HumanRecord()
            try:
                hr.id = record['id']
                hr.name = record['name']
                hr.address = record['address']
                hr.phone = record['phone']
                hr.email = record['email']
            except:
                print 'Invalid record: %s' % record
                continue

            self.records.append(hr)

    @classmethod
    def _elementToDict(cls, parent):
        child = parent.firstChild
        if not child:
            return None
        elif child.nodeType == xml.dom.minidom.Node.TEXT_NODE:
            nodeValue =  child.nodeValue
            return nodeValue

        dict_ = {}

        while child:
            if child.nodeType == xml.dom.minidom.Node.ELEMENT_NODE:

                if child.firstChild.nodeType == xml.dom.minidom.Node.TEXT_NODE:
                    dict_[child.tagName] = cls._elementToDict(child)

                else:
                    try:
                        dict_[child.tagName]
                    except KeyError:
                        dict_[child.tagName] = []

                    dict_[child.tagName].append(cls._elementToDict(child))

            child = child.nextSibling
        return dict_

    @classmethod
    def _buildTree(cls, doc, parent, structure):
        if type(structure) == dict:
            for k in structure:
                tag = doc.createElement(k)
                parent.appendChild(tag)
                cls._buildTree(doc, tag, structure[k])

        elif type(structure) == list:
            grandFather = parent.parentNode
            tagName = parent.tagName
            grandFather.removeChild(parent)
            for l in structure:
                tag = doc.createElement(tagName)
                cls._buildTree(doc, tag, l)
                grandFather.appendChild(tag)

        else:
            data = str(structure)
            tag = doc.createTextNode(data)
            parent.appendChild(tag)


    def write(self, filepath):
        print '%s plugin saving to %s' % (self.name, filepath)

        humanRecords = []

        for record in self.records:
            humanRecords.append(record.to_dict())

        records = {'doc': {'human': humanRecords}}

        doc = xml.dom.minidom.Document()
        rootName = str(records.keys()[0])
        root = doc.createElement(rootName)
        doc.appendChild(root)
        self._buildTree(doc, root, records[rootName])

        with open(filepath, 'w') as f:
            doc.writexml(f)