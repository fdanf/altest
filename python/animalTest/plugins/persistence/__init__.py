from animalTest.plugins import utils


# import persistence plugin classes here
from animalTest.plugins.persistence.baseIO import BaseIO
from animalTest.plugins.persistence.csvIO import CsvIO
from animalTest.plugins.persistence.jsonIO import JsonIO
from animalTest.plugins.persistence.xmlIO import XmlIO


# add new plugins here
PERSISTENCE_PLUGINS = [CsvIO, JsonIO, XmlIO]


def get_plugin(plugin_name, records):
    plugin_args = [records]
    return utils.get_plugin(plugin_name, PERSISTENCE_PLUGINS, BaseIO, *plugin_args)
