import json

from animalTest.plugins.persistence import baseIO
from animalTest import dataTypes


class JsonIO(baseIO.BaseIO):

    name = 'json'

    def __init__(self, records):
        super(JsonIO, self).__init__(records)

    def read(self, filepath):
        print '%s plugin loading %s' % (self.name, filepath)

        with open(filepath) as f:
            data = json.load(f)

        for record in data['records']:
            hr = dataTypes.HumanRecord()

            try:
                hr.id = record['id']
                hr.name = record['name']
                hr.address = record['address']
                hr.phone = record['phone']
                hr.email = record['email']
            except:
                print 'Invalid record: %s' % record
                continue

            self.records.append(hr)

    def write(self, filepath):
        print '%s plugin saving to %s' % (self.name, filepath)
        records = []

        for record in self.records:
            records.append(record.to_dict())

        jsonDict = {'records': records}

        with open(filepath, 'w') as f:
            json.dump(jsonDict, f)