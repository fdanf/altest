

class BaseIO(object):

    name = ''

    def __init__(self, records):
        self.records = records

    def read(self, filepath):
        raise NotImplementedError

    def write(self, filepath):
        raise NotImplementedError
