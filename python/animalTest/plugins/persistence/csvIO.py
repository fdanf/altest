from animalTest.plugins.persistence import baseIO
from animalTest import dataTypes


class CsvIO(baseIO.BaseIO):

    name = 'csv'

    def __init__(self, records):
        super(CsvIO, self).__init__(records)

    def read(self, filepath):
        print '%s plugin loading %s' % (self.name, filepath)

        with open(filepath, 'r') as f:
            for line in f:
                hr = dataTypes.HumanRecord()
                record = line.strip().split(',')

                try:
                    hr.id = record[0]
                    hr.name = record[1]
                    hr.address = record[2]
                    hr.phone = record[3]
                    hr.email = record[4]
                except IndexError:
                    print 'Invalid record: %s' % record
                    continue

                self.records.append(hr)

    def write(self, filepath):
        print '%s plugin saving to %s' % (self.name, filepath)

        lines = []

        for record in self.records:
            csvRecord = '%s,%s,%s,%s,%s' % (record.id, record.name, record.address, record.phone, record.email)
            lines.append(csvRecord)

        with open(filepath, 'w') as f:
            f.write('\n'.join(lines))