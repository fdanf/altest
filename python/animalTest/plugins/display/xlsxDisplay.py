from animalTest.plugins.display import baseDisplay
import xlsxwriter


class XlsxDisplay(baseDisplay.BaseDisplay):

    name = 'xlsx'

    def __init__(self, records):
        super(XlsxDisplay, self).__init__(records)

    def render(self, filepath=None):
        if not filepath:
            print 'this display plugin requires a filepath kwarg'
            return

        print '%s render file: %s' % (self.name, filepath)

        workbook = xlsxwriter.Workbook(filepath)
        worksheet = workbook.add_worksheet()

        columnMap = {'id': 'A', 'name': 'B', 'address': 'C', 'phone': 'D', 'email': 'E'}

        for key, value in columnMap.items():
            worksheet.write('%s1' % value, key)

        rowCounter = 2

        for record in self.records:
            for key, value in record.to_dict().items():
                column = columnMap[key]
                cell = '%s%s' % (column, rowCounter)
                worksheet.write('%s' % cell, value)

            rowCounter += 1

        workbook.close()
