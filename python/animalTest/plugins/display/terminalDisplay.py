from animalTest.plugins.display import baseDisplay

from pprint import pprint


class TerminalDisplay(baseDisplay.BaseDisplay):

    name = 'terminal'

    def __init__(self, records):
        super(TerminalDisplay, self).__init__(records)

    def render(self, filepath=None):

        for record in self.records:
            pprint(record.to_dict())