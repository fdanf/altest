from animalTest.plugins import utils


# import display plugins here
from animalTest.plugins.display.baseDisplay import BaseDisplay
from animalTest.plugins.display.htmlDisplay import HtmlDisplay
from animalTest.plugins.display.xlsxDisplay import XlsxDisplay
from animalTest.plugins.display.terminalDisplay import TerminalDisplay


# add new plugins here
DISPLAY_PLUGINS = [HtmlDisplay, XlsxDisplay, TerminalDisplay]


def get_plugin(plugin_name, records):
    plugin_args = [records]
    return utils.get_plugin(plugin_name, DISPLAY_PLUGINS, BaseDisplay, *plugin_args)
