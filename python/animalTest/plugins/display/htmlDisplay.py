import HTML

from animalTest.plugins.display import baseDisplay


class HtmlDisplay(baseDisplay.BaseDisplay):

    name = 'html'

    def __init__(self, records):
        super(HtmlDisplay, self).__init__(records)

    def render(self, filepath=None):
        if not filepath:
            print 'this display plugin requires a filepath kwarg'
            return

        print '%s render file: %s' % (self.name, filepath)

        table = HTML.Table(header_row=['id', 'name', 'phone', 'address', 'email'])

        for record in self.records:
            table.rows.append([record.id, record.name, record.phone, record.address, record.email])

        hthmlString = '<!DOCTYPE html>\n<html lang="en">\n <head>\n <meta charset="UTF-8">\n <title>Title</title>\n </head>\n <body>'
        hthmlString += str(table)
        hthmlString += '</body>\n</html>'

        with open(filepath, 'w') as f:
            f.write(hthmlString)
