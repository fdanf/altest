
class BaseDisplay(object):

    name = ''

    def __init__(self, records):
        self.records = records

    def render(self, filepath=None):
        raise NotImplementedError


