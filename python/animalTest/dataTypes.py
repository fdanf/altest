class HumanRecord(object):
    def __init__(self):
        self.id = ''
        self.name = ''
        self.address = ''
        self.phone = ''
        self.email = ''

    def to_dict(self):
        return self.__dict__

